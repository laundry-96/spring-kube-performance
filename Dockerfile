FROM openjdk:11-slim

ARG jarFile=build/libs/demo.jar

COPY ${jarFile} demo.jar

EXPOSE 8080

ENTRYPOINT exec java -jar demo.jar
