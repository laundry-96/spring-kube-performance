package com.testing.demo;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;


@RestController
public class HashController {

	static Random longGen = new Random();

	@RequestMapping(value="/compute/{id}")
	public String computeId(@PathVariable(value = "id") long userId) {
		System.out.println("Started");
		long beforeCompute = new Date().getTime();
		long[] memoryConsumption = new long[10];
		double[][] mathCons = new double[memoryConsumption.length][10000];
		String salt = BCrypt.gensalt(10);
		String hash = Long.toString(userId);
		for (int i = 0; i < 1; i++) {
			hash = BCrypt.hashpw(hash, salt);
		}

		for (int i = 0; i < memoryConsumption.length; i++) {
			memoryConsumption[i] = longGen.nextLong();
		}

		for (int i = 0; i < memoryConsumption.length; i++) {

			for(int j = 0; j < mathCons[0].length; j++) {
				mathCons[i][j] = Math.pow((double)memoryConsumption[i], longGen.nextInt(5));
			}
		}

		long afterCompute = new Date().getTime();

		System.out.println("This took " + (afterCompute - beforeCompute));

		return hash;
	}
}
