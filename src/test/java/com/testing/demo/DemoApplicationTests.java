package com.testing.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.net.http.HttpHeaders;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

  @Test
  public void testHash() {
    TestRestTemplate restTemplate = new TestRestTemplate();
    double hash = restTemplate.getForObject("http://localhost:8080/users/484753", long.class);
    System.out.println(hash);
  }
}
